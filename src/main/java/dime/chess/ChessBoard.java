package dime.chess;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by dime on 21/02/15.
 */
public class ChessBoard {
    /* The list of solvers use to solve the problem */
    private Solver[] solvers;

    /* A list of valid unique configurations */
    private Set<String> validUniqueConfigurations = new HashSet<>();

    /**
     * Default constructor
     *
     * @param m
     *          The number of rows of the chessboard
     * @param n
     *          The number of columns of the chessboard
     * @param kings
     *          The number of kings that need to be placed on the board
     * @param queens
     *          The number of queens that need to be placed on the board
     * @param bishops
     *          The number of bishops that need to be placed on the board
     * @param rooks
     *          The number of rooks that need to be placed on the board
     * @param knights
     *          The number of knights that need to be placed on the board
     */
    public ChessBoard(int m, int n, int kings, int queens, int bishops, int rooks, int knights) {

        // Save the start time
        long startTime = System.currentTimeMillis();

        // Create one solver for every position of the first piece
        solvers = new Solver[m * n];
        for (int i=0; i<solvers.length; i++) {
            // Create the solver
            solvers[i] = new Solver(m, n, kings, queens, bishops, rooks, knights, i);

            // Start the solver (separate thread)
            solvers[i].start();
        }

        // Wait for all the threads to finish
        for (int i=0; i<solvers.length; i++) {
            try {
                solvers[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Calculate the time needed to solve the problem
        long timeNeeded = System.currentTimeMillis() - startTime;

        // Gather all unique configurations in one final set - to remove possible duplicates
        for (int i=0; i<solvers.length; i++) {
            validUniqueConfigurations.addAll(solvers[i].getValidUniqueConfigurations());
        }

        // Print all unique configurations
        validUniqueConfigurations.forEach(c -> printConfiguration(m, n, c));

        // Print the number of unique configurations and the time needed
        System.out.println("Number of unique valid configurations " + validUniqueConfigurations.size() + ", Time = " + timeNeeded + " ms");
    }


    /**
     * Prints the current state of the chess board
     */
    private void printConfiguration(int m, int n, String configuration) {
        System.out.println("                 ");

        int charIndex = 0;
        for (int r = 0; r < m; r++) {
            for (int c = 0; c < n; c++) {
                if (c == 0) {
                    System.out.print("|");
                }
                System.out.print(configuration.charAt(charIndex++) + "|");
            }
            System.out.println();
        }

        System.out.println("                 ");
    }

    /**
     * Entry point
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 7) {
            System.out.println("USAGE: ChessBoard M N KINGS QUEENS BISHOPS ROOKS KNIGHTS");
            System.exit(1);
        }

        int[] input = new int[7];
        try {
            for (int i=0; i<input.length; i++) {
                input[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            System.err.println("All input arguments must be integer!");
            System.exit(1);
        }

        new ChessBoard(input[0], input[1], input[2], input[3], input[4], input[5], input[6]);
    }
}
