package dime.chess;

import dime.chess.pieces.Piece;
import dime.chess.pieces.implementations.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Created by dime on 02/03/15.
 */
public class Solver extends Thread {
    /* The fields of this chess board */
    private ChessField[][] fields;

    /* All the pieces on the board */
    private List<Piece> pieces;

    /* All the valid unique configurations for this solver */
    private Set<String> validUniqueConfigurations = new HashSet<>();

    /* The index of the solver. Needed for the position of the first piece */
    private int index;

    /* The number of rows of the chess board */
    private int m;

    /* The number of columns of the chess board */
    private int n;

    /**
     * Default constructor
     *
     * @param m
     * @param n
     * @param kings
     * @param queens
     * @param bishops
     * @param rooks
     * @param knights
     * @param index
     */
    public Solver(int m, int n, int kings, int queens, int bishops, int rooks, int knights, int index) {
        // Save the needed fields
        this.index = index;
        this.m = m;
        this.n = n;

        // First construct the board
        fields = new ChessField[m][n];
        for (int r=0; r<m; r++) {
            for (int c=0; c<n; c++) {
                fields[r][c] = new ChessField();
            }
        }

        // Construct the pieces
        pieces = new ArrayList<>(kings + queens + bishops + rooks + knights);
        IntStream.range(0, queens).forEach(i -> pieces.add(new Queen(fields)));
        IntStream.range(0, rooks).forEach(i -> pieces.add(new Rook(fields)));
        IntStream.range(0, bishops).forEach(i -> pieces.add(new Bishop(fields)));
        IntStream.range(0, kings).forEach(i -> pieces.add(new King(fields)));
        IntStream.range(0, knights).forEach(i -> pieces.add(new Knight(fields)));
    }

    @Override
    public void run() {
        // Sanity check
        if (pieces.isEmpty()) {
            // Nothing to do
            return;
        }

        // Position the first piece
        int row = index / n;
        int col = index % n;
        pieces.get(0).occupy(row, col);

        // Start solving from the second piece, because the first is already positioned and fixed for this solver
        solve(1);
    }

    /**
     * Solves the given problem.
     *
     * @param piecePosition
     */
    private void solve(int piecePosition) {
        // Check if some pieces left? If not - we have one valid combination
        if (piecePosition == pieces.size()) {
            saveUniqueConfiguration();
            return;
        }

        // Get the piece at the given position
        Piece piece = pieces.get(piecePosition);

        for (int r=0; r<fields.length; r++) {
            for (int c=0; c<fields[r].length; c++) {
                // Get the current chess field
                ChessField field = fields[r][c];

                // If the field is not free - leave it alone.
                if (!field.canPlacePiece()) {
                    continue;
                }

                // The field is free - try to occupy it
                if (piece.occupy(r, c)) {
                    // Successfully placed the piece on the field. Go into recursion...
                    solve(piecePosition + 1);
                }

                // Release the fields with the current piece
                piece.releaseFields();
            }
        }

        // Release the fields with the current piece
        piece.releaseFields();
    }

    public Set<String> getValidUniqueConfigurations() {
        return validUniqueConfigurations;
    }

    /**
     * Saves the current configuration in the list of valid configurations.
     * If such configuration exists - does nothing.
     */
    private void saveUniqueConfiguration() {
        // The config string
        StringBuilder stringBuilder = new StringBuilder();

        for (int r = 0; r < fields.length; r++) {
            for (int c = 0; c < fields[r].length; c++) {
                stringBuilder.append(fields[r][c].getOccupant() == null ? " " : fields[r][c].getOccupant());
            }
        }

        validUniqueConfigurations.add(stringBuilder.toString());
    }
}
