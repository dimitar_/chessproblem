package dime.chess.pieces.implementations;

import dime.chess.ChessField;
import dime.chess.pieces.Piece;

/**
 * Created by dime on 21/02/15.
 */
public class Bishop extends Piece {
    /**
     * The bishop has no restrictions in distance for each move,
     * but is limited to diagonal movement.
     */

    /**
     * Default constructor
     *
     * @param fields
     */
    public Bishop(ChessField[][] fields) {
        super(fields);
    }

    @Override
    public boolean occupy(int row, int column) {
        boolean success = super.occupy(row, column);
        success &= occupyDiagonal(row, column);

        if (!success) {
            // Rollback
            releaseFields();
            return false;
        }

        return true;
    }


    @Override
    public String toString() {
        return "B";
    }
}
