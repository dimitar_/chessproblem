package dime.chess.pieces.implementations;

import dime.chess.ChessField;
import dime.chess.pieces.Piece;

/**
 * Created by dime on 21/02/15.
 */
public class Queen extends Piece {
    /**
     * The queen can be moved any number of unoccupied squares in a straight line vertically,
     * horizontally, or diagonally, thus combining the moves of the rook and bishop.
     */

    /**
     * Default constructor
     *
     * @param fields
     */
    public Queen(ChessField[][] fields) {
        super(fields);
    }


    @Override
    public boolean occupy(int row, int column) {
        boolean success = super.occupy(row, column);
        success &= occupyHorizontalAndVertical(row, column);
        success &= occupyDiagonal(row, column);

        if (!success) {
            // Rollback
            releaseFields();
            return false;
        }

        return true;
    }



    @Override
    public String toString() {
        return "Q";
    }
}
