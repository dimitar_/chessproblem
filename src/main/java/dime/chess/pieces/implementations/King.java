package dime.chess.pieces.implementations;

import dime.chess.ChessField;
import dime.chess.pieces.Piece;

/**
 * Created by dime on 21/02/15.
 */
public class King extends Piece {
    /**
     * A king can move one square in any direction (horizontally, vertically, or diagonally).
     */

    /**
     * Default constructor
     *
     * @param fields
     */
    public King(ChessField[][] fields) {
        super(fields);
    }

    @Override
    public boolean occupy(int row, int column) {
        boolean success = super.occupy(row, column);

        if (success) {
            for (int r = Math.max(0, row - 1); r < Math.min(fields.length, row + 2); r++) {

                // No need to check further if we failed occupying some field
                if (!success) {
                    break;
                }

                for (int c = Math.max(0, column - 1); c < Math.min(fields[r].length, column + 2); c++) {

                    // Already taken care of in super.occupy(...)
                    if (r == row && c == column) {
                        continue;
                    }

                    // Sanity check
                    if (!isFieldFree(r, c)) {
                        success = false;
                        break;
                    }

                    // Occupy
                    fields[r][c].addIndirectOccupant(this);
                }
            }
        }

        if (!success) {
            // Rollback
            releaseFields();
            return false;
        }

        return true;
    }


    @Override
    public String toString() {
        return "K";
    }
}
