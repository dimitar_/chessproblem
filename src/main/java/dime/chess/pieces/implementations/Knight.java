package dime.chess.pieces.implementations;

import dime.chess.ChessField;
import dime.chess.pieces.Piece;

/**
 * Created by dime on 21/02/15.
 */
public class Knight extends Piece {
    /**
     *  It can move to a square that is two squares horizontally and one square vertically,
     *  or two squares vertically and one square horizontally.
     *  The complete move therefore looks like the letter L.
     */

    /**
     * Default constructor
     *
     * @param fields
     */
    public Knight(ChessField[][] fields) {
        super(fields);
    }

    @Override
    public boolean occupy(int row, int column) {
        boolean success  = super.occupy(row, column);

        success &= placeIndirectKnightAt(row - 2, column - 1);
        success &= placeIndirectKnightAt(row - 2, column + 1);

        success &= placeIndirectKnightAt(row - 1, column - 2);
        success &= placeIndirectKnightAt(row - 1, column + 2);

        success &= placeIndirectKnightAt(row + 1, column - 2);
        success &= placeIndirectKnightAt(row + 1, column + 2);

        success &= placeIndirectKnightAt(row + 2, column - 1);
        success &= placeIndirectKnightAt(row + 2, column + 1);

        if (!success) {
            // Rollback
            releaseFields();
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "N";
    }

    private boolean placeIndirectKnightAt(int r, int c) {
        if (areValidIndices(r, c)) {
            // Sanity check
            if (!isFieldFree(r, c)) {
                return false;
            }

            // Occupy
            fields[r][c].addIndirectOccupant(this);
        }

        // Return true, because the indices are invalid and we can count that as success.
        return true;
    }

    /**
     * Returns true if the given row & column indices exist.
     *
     * @param row
     * @param column
     * @return
     */
    private boolean areValidIndices(int row, int column) {
        return row >= 0 && row < fields.length && column >= 0 && column < fields[row].length;
    }
}
