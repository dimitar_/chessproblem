package dime.chess.pieces.implementations;

import dime.chess.ChessField;
import dime.chess.pieces.Piece;

/**
 * Created by dime on 21/02/15.
 */
public class Rook extends Piece {

    /**
     * The rook moves horizontally or vertically, through any number of unoccupied squares.
     */

    /**
     * Default constructor
     *
     * @param fields
     */
    public Rook(ChessField[][] fields) {
        super(fields);
    }


    @Override
    public boolean occupy(int row, int column) {
        boolean success = super.occupy(row, column);
        success &= occupyHorizontalAndVertical(row, column);

        if (!success) {
            // Rollback
            releaseFields();
            return false;
        }

        return true;
    }



    @Override
    public String toString() {
        return "R";
    }
}
