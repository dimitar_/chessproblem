package dime.chess.pieces;

import dime.chess.ChessField;

/**
 * Created by dime on 21/02/15.
 */
public abstract class Piece {

    // The chess board's fields
    protected ChessField[][] fields;

    /**
     * Default constructor
     *
     * @param fields
     */
    public Piece(ChessField[][] fields) {
        this.fields = fields;
    }

    /**
     * Occupies only the given field. For the indirect fields, the implementations are responsible.
     *
     * @param row
     * @param column
     */
    public boolean occupy(int row, int column) {
        // Sanity check
        if (!fields[row][column].canPlacePiece()) {
//            System.err.printf("Cannot occupy the field [%d][%d]. It's already occupied!\n", row, column);
            return false;
        }

        // Occupy
        fields[row][column].setOccupant(this);
        return true;
    }


    /**
     * Checks if the field is free. If not, prints an error message.
     *
     * @return
     */
    protected boolean isFieldFree(int r, int c) {
        if (!fields[r][c].isFree()) {
//            System.err.printf("Cannot indirectly occupy the field [%d][%d]. It's already occupied!\n", r, c);
            return false;
        }

        return true;
    }


    /**
     * Occupies the diagonal fields
     *
     * @param row
     * @param column
     * @return
     */
    protected boolean occupyDiagonal(int row, int column) {
        int x = row - column; // First diagonal check
        int y = row + column; // Second diagonal check

        for (int r = 0; r < fields.length; r++) {
            for (int c = 0; c < fields[r].length; c++) {

                // Already taken care of in super.occupy(...)
                if (r == row && c == column) {
                    continue;
                }


                if (r - c == x || r + c == y) {

                    // Sanity check
                    if (!isFieldFree(r, c)) {
                        return false;
                    }

                    // Occupy
                    fields[r][c].addIndirectOccupant(this);
                }
            }
        }

        return true;
    }

    /**
     * Occupies the horizontal and vertical fields.
     *
     * @param row
     * @param column
     * @return
     */
    protected boolean occupyHorizontalAndVertical(int row, int column) {
        // All columns in the row
        for (int c = 0; c < fields[row].length; c++) {
            // Already taken care of in super.occupy(...)
            if (c == column) {
                continue;
            }

            if(!isFieldFree(row, c)) {
                return false;
            }

            // Occupy
            fields[row][c].addIndirectOccupant(this);
        }

        // All rows in the column
        for (int r = 0; r < fields.length; r++) {
            // Already taken care of in super.occupy(...)
            if (r == row) {
                continue;
            }

            if(!isFieldFree(r, column)) {
                return false;
            }

            // Occupy
            fields[r][column].addIndirectOccupant(this);
        }

        return true;
    }

    /**
     * Releases all fields
     */
    public void releaseFields() {
        for (int r = 0; r < fields.length; r++) {
            for (int c = 0; c < fields[r].length; c++) {
                if (fields[r][c].getOccupant() == this) {
                    fields[r][c].setOccupant(null);
                } else if (fields[r][c].hasIndirectOccupant(this)) {
                    fields[r][c].removeIndirectOccupant(this);
                }
            }
        }
    }
}
