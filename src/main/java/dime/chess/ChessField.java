package dime.chess;

import dime.chess.pieces.Piece;

import java.util.ArrayList;
import java.util.List;

/**
 * One field in the chess board
 *
 * Created by dime on 21/02/15.
 */
public class ChessField {

    /* The piece positions on this field. Null if none. */
    private Piece occupant;

    /* The pieces that can move to this field. Can be empty */
    private List<Piece> indirectOccupants = new ArrayList<>();

    /**
     * Sets the direct occupant of this field.
     *
     * @param occupant
     */
    public void setOccupant(Piece occupant) {
        this.occupant = occupant;
    }

    /**
     * Adds an indirect occupant of this field
     *
     * @param indirectOccupant
     */
    public void addIndirectOccupant(Piece indirectOccupant) {
        indirectOccupants.add(indirectOccupant);
    }

    /**
     * Removes the given piece form the list of indirect occupants. Nothing happens if the piece is not in the list.
     * @param indirectOccupant
     */
    public void removeIndirectOccupant(Piece indirectOccupant) {
        indirectOccupants.remove(indirectOccupant);
    }

    /**
     * Returns the direct occupant
     *
     * @return
     */
    public Piece getOccupant() {
        return occupant;
    }

    /**
     * Returns the indirect occupant
     *
     * @return
     */
    public boolean hasIndirectOccupant(Piece testPiece) {
        return indirectOccupants.contains(testPiece);
    }

    /**
     * Returns true if a piece can be placed on this field.
     *
     * @return
     */
    public boolean canPlacePiece() {
        return occupant == null && indirectOccupants.isEmpty();
    }

    /**
     * Returns true if the field is free (doesn't have direct occupant)
     *
     * @return
     */
    public boolean isFree() {
        return occupant == null;
    }

}
